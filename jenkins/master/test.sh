docker run  --rm --name jenkins2 \
-p 8080:8080 \
-v ${PWD}/Jenkins-JCasC.yml:/var/jenkins_home/conf/Jenkins-JCasC.yml \
-v ${PWD}/repository/:/root/.m2/repository \
jenkins-master
