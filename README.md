# 基於Docker建立的DevOps環境

## 前置動作
1. 確認Docker資源至少有CPU * 2 & Memory > 3GB
1. 進入 `jenkins/master` 目錄
1. 執行 `download.sh` 
1. 執行 `docker build -t jenkins-master .`

**由於Jenkins Plugin Repository偶有不穩定的情況，因此如遇到Plugin無法下載的錯誤，可以幾分鐘後再試**

## 啟動方式
### MacOS 執行以下指令
```
ENV_PATH=$(pwd)/opt docker-compose up
```

```
ENV_PATH=$(pwd)/opt docker-compose -f docker-compose-git.yml up
```

### Windows 執行以下指令(TODO)
```
set ENV_PATH=%CD%/opt 
docker-compose up
```

## 環境使用

### Jenkins
- http://127.0.0.1:8082/jenkins

### Sonarqube]
- http://127.0.0.1:9000/projects?sort=-analysis_date
- NAME/PWD: admin / admin

### Nexus (Maven Repository)
- http://127.0.0.1:8081/nexus/
- NAME/PWD: admin / admin123

### Redmine (TODO)
- IP: http://127.0.0.1:8083/redmine)
- NAME/PWD:  user / bitnami1


### 測試用
### 測試用指令

- 全部清除再啟動
```
export ENV_PATH=$(pwd)/opt
docker stop jenkins nexus sonarqube postgresql redmine
docker rm jenkins nexus sonarqube postgresql redmine
docker ps -a
rm -rf opt && docker-compose up
```

- 進入docker container

`docker exec -it jenkins2 bash`

`docker exec -it sonarqube bash`


### reference
- https://github.com/muralibala/docker-compose-jenkins-nexus-sonar/blob/master/docker-compose.yml

- https://codebabel.com/sonarqube-with-jenkins/

- https://github.com/jenkinsci/blueocean-plugin/tree/master/docker-demo

- https://code-maze.com/ci-jenkins-docker/

- https://github.com/stefanprodan/jenkins/blob/master/Dockerfile

- https://dzone.com/articles/dockerizing-jenkins-2-setup-and-using-it-along-wit

- https://pghalliday.com/jenkins/groovy/sonar/chef/configuration/management/2014/09/21/some-useful-jenkins-groovy-scripts.html

- https://codebabel.com/jcasc-stateless-ci/
 
- https://github.com/jenkinsci/configuration-as-code-plugin/issues/454
- https://github.com/openshift/test-maven-app.git

